## Get settlement details
```
request(
    "GET",
    "/settlement/info",
    {
        "hash_token_id": 48,
        "date": "2019-11-04"
    }
)
```

## Get subscription info
```
request(
    "GET",
    "/subscription/info",
    {
        "hash_token_id": 48
    }
)
```

## Order hash token
```
request(
    "POST",
    "/subscription/order",
    {
        "hash_token_id": 48,
        "sales_batch_id": 11,
        "pay_currency": "btc",
        "amount": 330
    }
)
```

## List orders
```
request(
    "GET",
    "/subscription/orders",
    {
        "hash_token_id": 48
    }
)
```