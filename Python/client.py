import functools
import hashlib
import hmac
import json
import random
import time
from binascii import b2a_hex, a2b_hex
from cmd import Cmd
import secrets

try:
    from urllib.parse import urlencode
except Exception:
    from urllib import urlencode

from pycoin.key import Key

from pycoin.encoding import public_pair_to_sec, to_bytes_32, from_bytes_32

import requests


HOST="https://staging.hashnest.com/open/api/v1"
API_KEY = "xxxx"
API_SECRET = "xxxx"
HASHNEST_PUB = 'xxxx'


def generate_new_key():
    secret = secrets.randbits(256)
    secret_hex = b2a_hex(to_bytes_32(secret)).decode()
    key = Key(secret_exponent=secret)
    sec = public_pair_to_sec(key.public_pair())
    return b2a_hex(sec).decode(), secret_hex


def generate_ecc_signature(content, key):
    key = Key(secret_exponent=from_bytes_32(a2b_hex(key)))
    return b2a_hex(
        key.sign(double_hash256(content))
    ).decode()

def double_hash256(content):
    return hashlib.sha256(
        hashlib.sha256(content.encode()).digest()).digest()


def sort_params(params):
    params = [(key, val) for key, val in params.items()]

    params.sort(key=lambda x: x[0])
    return urlencode(params)


def verify_response(response):
    content = response.content.decode()
    success = True
    try:
        nonce = response.headers['Biz-Api-Nonce']
        signature = response.headers['Biz-Api-Signature']
        success = verify('%s|%s' % (nonce, content), signature, HASHNEST_PUB)
    except KeyError:
        pass

    return success, json.loads(content)


def verify(content, signature, pub_key):
    key = Key.from_sec(a2b_hex(pub_key))
    return key.verify(double_hash256(content),
                      a2b_hex(signature))


def request(method, path, params, api_key=API_KEY, api_secret=API_SECRET, host=HOST):
    nonce = str(int(time.time() * 1000))
    content = '%s|%s' % (nonce, sort_params(params))
    signature = generate_ecc_signature(content, api_secret)
    
    headers = {
        'Biz-Api-Key': api_key,
        'Biz-Api-Nonce': nonce,
        'Biz-Api-Signature': signature
    }
    if method == 'GET':
        resp = requests.get('%s%s' % (host, path), params=urlencode(params),
                            headers=headers)
    elif method == 'POST':
        resp = requests.post('%s%s' % (host, path), data=params,
                             headers=headers)
    else:
        raise Exception("Not support http method")
    verify_success, result = verify_response(resp)
    if not verify_success:
        raise Exception(
            'Fatal: verify content error, maybe encounter mid man attack')
    return result
