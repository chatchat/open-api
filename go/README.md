## Get settlement details

```
Request("GET", "/settlement/info", map[string]string{
    "hash_token_id": "48",
    "date": "2019-11-04",
})
```

## Get subscription info

```
Request("GET", "/subscription/info", map[string]string{
    "hash_token_id": "48",
})
```

## Order hash token

```
Request("POST", "/subscription/order", map[string]string{
    "hash_token_id": "48",
    "sales_batch_id": "11",
    "pay_currency": "btc",
    "amount": "330",
})
```

## List orders

```
Request("GET", "/subscription/orders", map[string]string{
    "hash_token_id": "48",
})
```
