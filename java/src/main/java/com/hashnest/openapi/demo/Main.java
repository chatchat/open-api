package com.hashnest.openapi.demo;

import okhttp3.*;
import okio.ByteString;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.Sha256Hash;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.TreeMap;

public class Main {
    private static OkHttpClient HTTP_CLIENT = new OkHttpClient();

    private static String DEFAULT_PUBKEY = "xxxx";

    private static byte[] doubleSha256(String content) {
        return Sha256Hash.hashTwice(content.getBytes());
    }

    private static byte[] hex2bytes(String s) {
        return ByteString.decodeHex(s).toByteArray();
    }

    private static String bytes2Hex(byte[] b) {
        return ByteString.of(b).hex();
    }

    private static String generateHmac256Signature(String content, String key) {
        ByteString k = ByteString.of(key.getBytes());
        return ByteString.of(content.getBytes()).hmacSha256(k).hex();
    }

    private static String generateEccSignature(String content, String key) {
        ECKey eckey = ECKey.fromPrivate(hex2bytes(key));
        return bytes2Hex(eckey.sign(Sha256Hash.wrap(doubleSha256(content))).encodeToDER());
    }

    private static String composeParams(TreeMap<String, Object> params) {
        StringBuffer sb = new StringBuffer();
        params.forEach((s, o) -> {
            try {
                sb.append(s).append("=").append(URLEncoder.encode(String.valueOf(o), "UTF-8")).append("&");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        });
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    private static boolean verifyResponse(String content, String sig, String pubkey) throws Exception {
        ECKey key = ECKey.fromPublicOnly(hex2bytes(pubkey));
        return key.verify(doubleSha256(content), hex2bytes(sig));
    }

    private static String request(String method, String path, TreeMap<String, Object> params, String apiKey, String apiSecret, String host) throws Exception {
        method = method.toUpperCase();
        String nonce = String.valueOf(System.currentTimeMillis());

        String paramString = composeParams(params);

        String content = nonce + "|" + paramString;

        String signature = generateEccSignature(content, apiSecret);

        Request.Builder builder = new Request.Builder()
                .addHeader("Biz-Api-Key", apiKey)
                .addHeader("Biz-Api-Nonce", nonce)
                .addHeader("Biz-Api-Signature", signature);
        Request request;
        if ("GET".equalsIgnoreCase(method)) {
            request = builder
                    .url(host + path + "?" + paramString)
                    .build();
        } else if ("POST".equalsIgnoreCase(method)) {
            FormBody.Builder bodyBuilder = new FormBody.Builder();
            params.forEach((s, o) -> bodyBuilder.add(s, String.valueOf(o)));
            RequestBody formBody = bodyBuilder.build();
            request = builder
                    .url(host + path)
                    .post(formBody)
                    .build();
        } else {
            throw new RuntimeException("not supported http method");
        }
        try (Response response = HTTP_CLIENT.newCall(request).execute()) {
            String nonce = response.header("Biz-Api-Nonce");
            String sig = response.header("Biz-Api-Signature");
            String body = response.body().string();
            System.out.println(body);
            boolean verifyResult = verifyResponse(nonce + "|" + body, sig, DEFAULT_PUBKEY);
            System.out.println(verifyResult);
            if (!verifyResult) {
                throw new RuntimeException("verify response error");
            }
            return body;
        }
    }

    public static void main(String... args) throws Exception {
        testApi();
    }

    public static void testApi() throws Exception {
        TreeMap<String, Object> params = new TreeMap<>();
        params.put("hash_token_id", 48);
        String apiKey = "xxxx";
        String apiSecret = "xxxx";
        String host = "https://staging.hashnest.com/open/api/v1";
        String res = request("GET", "/subscription/info", params, apiKey, apiSecret, host);
        System.out.println(res);
    }
}


